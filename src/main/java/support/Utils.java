package support;

public class Utils {
	/**
	 * Use for Numbering Sequence in JSP
	 * 
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public static int sequenceForList(int page, int pageSize) {
		return ((page - 1) * pageSize);
	}
	
	/**
	 * Checking String Empty or not
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}
}

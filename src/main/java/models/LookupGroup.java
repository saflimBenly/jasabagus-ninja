package models;

import javax.persistence.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by safaridinsal on 28/07/2015.
 */
@Entity
public class LookupGroup extends BaseModel<Long> {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;
	private String code;
	private String description;
	@OneToMany(cascade = CascadeType.ALL, 
    		mappedBy = "lookupGroup", orphanRemoval = true, targetEntity=Lookup.class, fetch=FetchType.EAGER)
	private Collection<Lookup> lookup;
	
    public LookupGroup() {}

    public LookupGroup(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Lookup> getLookup() {
        return lookup;
    }

    public void setLookup(Collection<Lookup> lookup) {
        this.lookup = lookup;
    }

    
    
}

package models;

import javax.persistence.*;

/**
 * Created by safaridinsal on 28/07/2015.
 */
@Entity
public class Lookup extends BaseModel<Long> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String code;
    private String name;
    private String description;
    private String use_yn;
    
    
    @ManyToOne(optional = false)
    private LookupGroup lookupGroup;

    public Lookup() {
    }

    public Lookup(Long lookupgroup_id, String code, String name, String description, String useYn) {
    	this.code = code;
        this.name = name;
        this.description = description;
        this.use_yn = useYn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String lookupCode) {
        this.code = lookupCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String lookupName) {
        this.name = lookupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LookupGroup getLookupGroup() {
        return lookupGroup;
    }

    public void setLookupGroup(LookupGroup lookupGroup) {
        this.lookupGroup = lookupGroup;
    }

	public String getUse_yn() {
		return use_yn;
	}

	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}

	

    
}

package models;

import javax.persistence.*;

/**
 * Created by safaridinsal on 31/07/2015.
 */
@Entity
public class Carousel extends BaseModel<Long> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String title;
    private String tagline;
    private String url;
    @ManyToOne
    private MainBillboard mainbillboard;
    @OneToOne
    private ServiceProduct serviceproduct;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public MainBillboard getMainbillboard() {
        return mainbillboard;
    }

    public void setMainbillboard(MainBillboard mainbillboard) {
        this.mainbillboard = mainbillboard;
    }

    public ServiceProduct getServiceproduct() {
        return serviceproduct;
    }

    public void setServiceproduct(ServiceProduct serviceproduct) {
        this.serviceproduct = serviceproduct;
    }
}

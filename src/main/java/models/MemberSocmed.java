package models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by safaridinsal on 31/07/2015.
 */
@Entity
public class MemberSocmed extends BaseModel<Long> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String socmedno;
    private String resfield1;
    private String resfield2;
    private String resfield3;
    private String resfield4;
    
    @ManyToOne
    private Member member;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSocmedno() {
        return socmedno;
    }

    public void setSocmedno(String socmedno) {
        this.socmedno = socmedno;
    }

    public String getResfield1() {
        return resfield1;
    }

    public void setResfield1(String resfield1) {
        this.resfield1 = resfield1;
    }

    public String getResfield2() {
        return resfield2;
    }

    public void setResfield2(String resfield2) {
        this.resfield2 = resfield2;
    }

    public String getResfield3() {
    	if (this.resfield3 == null) {
    		return "";
    	} else {
            return resfield3;
    	}
    }

    public void setResfield3(String resfield3) {
        this.resfield3 = resfield3;
    }

    public String getResfield4() {
    	if (this.resfield4 == null) {
    		return "";
    	} else {
            return resfield4;
    	}
    }

    public void setResfield4(String resfield4) {
        this.resfield4 = resfield4;
    }

    @JsonIgnore
    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
    
    public MemberSocmed() {
		super();
    }

    public MemberSocmed(Long id) {
		super();
		this.id = id;
		this.socmedno = "";
		this.resfield1 = "";
		this.resfield2 = "";
		this.resfield3 = "";
		this.resfield4 = "";
	}
}

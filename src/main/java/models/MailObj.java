package models;

import java.sql.Date;

public class MailObj {

	private String subject;
	private String from;
	private String[] addTo;
	private String[] addCc;
	private String[] addBcc;
	private String bodyHtml;
	private String bodyText;
	
	public MailObj(String subject, String from, String[] addTo, String bodyHtml, String bodyText) {
		this.setSubject(subject);
		this.setFrom(from);
		
		this.setAddTo(addTo);
		
		this.setBodyHtml(bodyHtml);
		
		this.setBodyText(bodyText);
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String[] getAddTo() {
		return addTo;
	}
	public void setAddTo(String[] addTo) {
		this.addTo = addTo;
	}
	public String[] getAddCc() {
		return addCc;
	}
	public void setAddCc(String[] addCc) {
		this.addCc = addCc;
	}
	public String[] getAddBcc() {
		return addBcc;
	}
	public void setAddBcc(String[] addBcc) {
		this.addBcc = addBcc;
	}
	public String getBodyHtml() {
		return bodyHtml;
	}
	public void setBodyHtml(String bodyHtml) {
		this.bodyHtml = bodyHtml;
	}

	public String getBodyText() {
		return bodyText;
	}

	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}
	
}

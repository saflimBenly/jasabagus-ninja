package models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by safaridinsal on 31/07/2015.
 */
@Entity
public class Category extends BaseModel<Long> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String code;
    private String description;
    private Long parent;
    private Integer level;
    @OneToMany
    private Set<ServiceProduct> serviceproduct;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Set<ServiceProduct> getServiceproduct() {
        return serviceproduct;
    }

    public void setServiceproduct(Set<ServiceProduct> serviceproduct) {
        this.serviceproduct = serviceproduct;
    }
}

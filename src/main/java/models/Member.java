package models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Member extends BaseModel<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    private String fullname;
    private boolean isAdmin;
    private String fbid;
    private String firstname;
    private String lastname;
    @Temporal(TemporalType.DATE)
    private Date birthdate;
    private String gender;
    private boolean fblogin;
    private String salt;
    @OneToMany(cascade = CascadeType.ALL,
            	mappedBy = "member", 
            	orphanRemoval = true, 
            	targetEntity = MemberSocmed.class, 
            	fetch = FetchType.EAGER)
    private Set<MemberSocmed> socmed;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "member", 
            orphanRemoval = true, 
            targetEntity = Address.class, 
            fetch = FetchType.EAGER)
    private Set<Address> address;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "member", orphanRemoval = true, targetEntity = Contact.class, fetch = FetchType.EAGER)
    private Set<Contact> contact;
    private String profile;
    @Transient
    private String dobString;
    
    private byte[] image;

    private String status;
    
    private String certificate;
    
    private String imagepath;
    
    public Member() {
    }

    public Member(String username, String password, String fullname) {
        this.setUsername(username);
        this.setPassword(password);
        this.setFullname(fullname);
    }

    public Member(String username, String password, String fullname, Date birthdate, String firstname, String lastname, String status) {
        this.setUsername(username);
        this.setPassword(password);
        this.setFullname(fullname);

        this.setBirthdate(birthdate);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setStatus(status);
    }

	public Member(String username, String firstname, String lastname, String fbid, String status){
		this.setUsername(username);
		this.setFirstname(firstname);
		this.setLastname(lastname);
		this.setFbid(fbid);
		this.setStatus(status);
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getFbid() {
		return fbid;
	}

	public void setFbid(String fbid) {
		this.fbid = fbid;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getGender() {
		if (this.gender == null || "".equals(this.gender)) gender = "T";
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isFblogin() {
		return fblogin;
	}

	public void setFblogin(boolean fblogin) {
		this.fblogin = fblogin;
	}

	public Set<MemberSocmed> getSocmed() {
		if (socmed.size() < 1) {
			socmed.add(new MemberSocmed(new Long(-1)));
		}
		return socmed;
	}

	public void setSocmed(Set<MemberSocmed> socmed) {
		this.socmed = socmed;
	}

	public Set<Address> getAddress() {
		if (address.size() < 1) {
			address.add(new Address(new Long(-1)));
		}
		return address;
	}

	public void setAddress(Set<Address> address) {
		this.address = address;
	}

	public Set<Contact> getContact() {
		if (contact.size() < 1) {
			contact.add(new Contact(new Long(-1)));
		}
		return contact;
	}

	public void setContact(Set<Contact> contact) {
		this.contact = contact;
	}

	public String getDobString() {
		SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
		if (this.birthdate == null) {
			return "";
		} else {
			return sdf.format(birthdate);
		}
	}

	public void setDobString(String dobString) {
		this.dobString = dobString;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

	
}

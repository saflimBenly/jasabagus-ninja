/**
 * Copyright (C) 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.AuthenticationUtil;
import dao.GenericDao;
import dao.SetupDao;
import filters.AuthenticationFilter;
import filters.CrossOriginAccessControlFilter;
import models.Article;
import models.Blog;
import models.FacebookAuthenticateRequest;
import models.Person;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.params.PathParam;
import ninja.utils.NinjaProperties;

import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.User;

@Singleton
@FilterWith(CrossOriginAccessControlFilter.class)
public class ApplicationController {

    @Inject 
    private NinjaProperties ninjaProperties;
    
    @Inject
    GenericDao dao;

    @Inject
    SetupDao setupDao;

    public ApplicationController() {

    }

    /**
     * Method to put initial data in the db...
     * 
     * @return
     */

        public Result setup() {

        setupDao.setup();

        return Results.ok();

    }

    public Result cors() {
        return Results.json();
    }
    
    public Result person(Person person) {
        person.setName(person.getName()+ " - edited");
        person.setEmail(person.getEmail() + " - edited");
        return Results.json().render(person);
    }
	
    public Result landing(){
        return Results.html();
	}
	
	public Result terms(){
        return Results.html();
	}

	public Result search(){
        return Results.html();
	}

	public Result index() {

        Article frontPost = dao.findFirst(Article.class);

        List<Article> olderPosts = dao.findByPage(Article.class,0,10);

        Map<String, Object> map = Maps.newHashMap();
        map.put("frontArticle", frontPost);
        map.put("olderArticles", olderPosts);

        return Results.html().render("frontArticle", frontPost)
                .render("olderArticles", olderPosts);

    }

    public Result authenticate(FacebookAuthenticateRequest request) {
        FacebookClient facebookClient = new DefaultFacebookClient(request.getToken(), ninjaProperties.get("facebook.secret"));
        User user = facebookClient.fetchObject("me", User.class);
        boolean authenticated = user.getEmail().equalsIgnoreCase(request.getEmail());
        if(authenticated) {
            String token = AuthenticationUtil.generateToken(user.getEmail(), ninjaProperties.get("application.secret"));
            HashMap<String, String> authorisationMap = new HashMap<String, String>();
            authorisationMap.put(AuthenticationUtil.tokenHeader, token);
            authorisationMap.put("userEmail", user.getEmail());
            return Results.json().render(authorisationMap);
        } else {
            return Results.unauthorized();
        }
    }
    
    @FilterWith(AuthenticationFilter.class)
    public Result saveBlog(Blog blog) {
        blog.setTitle(blog.getTitle()+ " - edited");
        blog.setContent(blog.getContent() + " - edited");
        return Results.json().render(blog);
    }
    
    @FilterWith(AuthenticationFilter.class)
    public Result getBlog(@PathParam("id") int id) {
        Blog blog = new Blog();
        blog.setId(id);
        blog.setTitle("CORS Stateless REST Service with Facebook Authentication");
        blog.setContent("How to authenticate a HTML5 client with a service using Facebook authentication");
        return Results.json().render(blog);
    }
    
}

/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import models.Address;
import models.Contact;
import models.EnumOrder;
import models.Lookup;
import models.LookupGroup;
import models.Member;
import models.MemberSocmed;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.params.Param;
import ninja.params.Params;
import ninja.params.PathParam;
import ninja.session.FlashScope;
import ninja.validation.JSR303Validation;
import ninja.validation.Validation;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import support.Constants;
import support.Utils;
import support.Pagination;
import dao.GenericDao;
import dao.LookupGroupDao;
import dao.MemberDao;
import dto.MemberDto;

@Singleton
public class AdminController{
	
	@Inject
	MemberDao memberDao;
	
	@Inject
	LookupGroupDao lookupGroupDao;
	
	@Inject
	GenericDao dao;
	
	@Inject 
	HttpServletRequest request;
	
	@Inject 
	HttpServletResponse response;

	private Log log = LogFactory.getLog(AdminController.class);   
	
	
	public Result adminFront() {
		return Results.html();	
	}
	
	public Result memberList(Context context) {
		String page = context.getParameter("page");
		Map<String, Map> paging = new HashMap<String, Map>();
		
		List<Member> members = null;
		String pagination="";
		int number = 0;
		try {
			int pageSize = Constants.PageSize;
			Integer paged = Integer
					.valueOf(page != null ? page : "1");
			number = Utils.sequenceForList(paged, pageSize);
			members = dao.findByPage(Member.class, paged, pageSize);
			int total = dao.countAll(Member.class);
			pagination = Pagination.generatePage(total, paged, pageSize);
		} catch (Exception e) {
			// TODO: handle exception
		}
		Result result = Results.html();
		result.render("listMember", members);
		result.render("pagination", pagination);
		result.render("number", number);
		System.out.println(pagination);
		return result;
	}
	
	public Result memberDetail(@PathParam("id") Long id) {
		
		Member member = dao.find(Member.class, id);
				
		Map<String, Map> dropdown = new HashMap<String, Map>();
		HashMap<String, String> genderLookup = new HashMap<String, String>();
		genderLookup.put("L", "Laki-laki");
		genderLookup.put("P", "Perempuan");
		genderLookup.put("T", "Tidak Tahu");
		
		dropdown.put("gender", genderLookup);
		
		Result result = Results.html()
				.render("member", member)
				.render("dropdown", dropdown);
	    return result;
		
	}
	public Result updateMember(Context context, @JSR303Validation MemberDto memberDto,
			Validation validation){
		try {      
	        
			//DateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
			//String string = "Tue Aug 25 2015";
			DateFormat format = new SimpleDateFormat("EEE MMMM dd yyyy", Locale.ENGLISH);
			//Date date = format.parse(string);
			//System.out.println(date); // Sat Jan 02 00:00:00 GMT 2010
			
			Member member = new Member();
			member = dao.find(Member.class, memberDto.id);
			
			member.setUsername(memberDto.username);
			member.setPassword(memberDto.password);
			member.setFullname(memberDto.fullname);
			member.setAdmin(memberDto.isAdmin);
			member.setFbid(memberDto.snsId);
			member.setFirstname(memberDto.firstname);
			member.setLastname(memberDto.lastname);
			if(memberDto.birthdate1!=null){ 
				member.setBirthdate(format.parse(memberDto.birthdate1));
			}
			member.setGender(memberDto.gender);
			member.setFblogin(memberDto.fblogin);
			
			Address addr = new Address();
			Set<Address> addrs = new HashSet<Address>();
			addr.setAddresstype(memberDto.addresstype);
			addr.setAddressline1(memberDto.addressline1);
			addr.setAddressline2(memberDto.addressline2);
			addr.setDistrict(memberDto.district);
			addr.setCity(memberDto.city);
			addr.setProvince(memberDto.province);
			addr.setCountry(memberDto.country);
			addr.setPostcode(memberDto.postcode);
			addr.setMember(member);
			addrs.add(addr);
			//member.getAddress().add(addr);
			member.setAddress(addrs);
			
			MemberSocmed socmed = new MemberSocmed();
			Set<MemberSocmed> socmeds = new HashSet<MemberSocmed>();
			socmed.setSocmedno(memberDto.socmedno);
			socmed.setResfield1(memberDto.resfield1);
			socmed.setResfield2(memberDto.resfield2);
			socmed.setResfield3(memberDto.resfield3);
			socmed.setResfield4(memberDto.resfield4);
			socmed.setMember(member);
			socmeds.add(socmed);
			member.setSocmed(socmeds);
			
			Contact contact = new Contact();
			contact.setContacttype(memberDto.contacttype);
			contact.setDetail(memberDto.detail);
			contact.setChannel(memberDto.channel);
			contact.setMember(member);
			Set<Contact> contacts = new HashSet<Contact>();
			contacts.add(contact);
			
			member.setContact(contacts);
			
			
			dao.merge(member);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		context.getFlashScope().success("success.member.update");
		return Results.redirect("/admin/member");
	}
	
	//upload image to binary
	/*public Result uploadImage(Context context) throws Exception {
		
		Member member = new Member();
		
		if(context.isMultipart()){

			FileItemIterator fileItemIterator = context.getFileItemIterator();

			while (fileItemIterator.hasNext()) {
				FileItemStream item = fileItemIterator.next();
				String name = item.getFieldName();
				InputStream stream = item.openStream();
				String contentType = item.getContentType();
				
				if (item.isFormField()) {
					if(name.equalsIgnoreCase("id")){
						Member mem = dao.find(Member.class, Long.parseLong(Streams.asString(stream)));
						member = mem;
					}
					
				} else {
					// process file as input stream
					try {
						byte[] bytes = IOUtils.toByteArray(stream);
						stream.read(bytes);
						member.setImage(bytes);
					} catch (Exception e) {
						e.printStackTrace();
					}


				}
			}
		}
		
		dao.merge(member);
		context.getFlashScope().success("success.image.upload");
		return Results.redirect("/admin/memberDetail/"+member.getId());
	}*/
	
	//upload image to directory
	public Result uploadImage(Context context) throws Exception {
		
		Member member = new Member();

		if(context.isMultipart()){

			FileItemIterator fileItemIterator = context.getFileItemIterator();

			while (fileItemIterator.hasNext()) {
				FileItemStream item = fileItemIterator.next();
				String name = item.getFieldName();
				InputStream inputStream = item.openStream();
				//String contentType = item.getContentType();
				
				if (item.isFormField()) {
					if(name.equalsIgnoreCase("id")){
						Member mem = dao.find(Member.class, Long.parseLong(Streams.asString(inputStream)));
						member = mem;
					}
					
				} else {
					// process file as input stream
					try {
						String imageName = item.getName();
						if(imageName != null && imageName != ""){
							
							String path = Constants.PATH+"/images/";
							File fileSaveDir = new File(path);
							if (!fileSaveDir.exists()) {
								fileSaveDir.mkdir();
						    }
							
							path = path+"/profile/";
							fileSaveDir = new File(path);
							if (!fileSaveDir.exists()) {
								fileSaveDir.mkdir();
						    }

							fileSaveDir = new File(path+member.getId()+"_version.png");	
			
							OutputStream output = new FileOutputStream(fileSaveDir);
							Streams.copy(inputStream, output, true);
											
							inputStream.close();
							output.close();
							
							member.setImagepath("/images/profile/"+member.getId()+"_version.png");
						}else{
							member.setImagepath("");
						}

					} catch (Exception e) {
						e.printStackTrace();
					}


				}
			}
		}
		
		dao.merge(member);
		context.getFlashScope().success("success.image.upload");
		return Results.redirect("/admin/memberDetail/"+member.getId());
	}
	
	public Result retriveImage(@PathParam("id") Long id, Context context) throws Exception {
		Member member = new Member();
		member = dao.find(Member.class, id);
		
		byte[] bImage = new byte[1024];
		bImage = member.getImage();
		
		Result resultImg = Results.contentType("image/jpeg").renderRaw(bImage);
		return resultImg;
	}
	

	
  /*  public Result lookupList() {
    	List<Category> categories = categoryDao.getAllCategory();
		return Results.html().render("listCategory", categories);
    }*/
	
	
	
	
	/**
	 * aad
	 * @return
	 */
    public Result lookupGroupList(
    		@Param("pageNumber") int pageNumber ,
    		@Param("pageIndex") int pageIndex,
    		@Param("pageSize") int pageSize) {
    	//List<LookupGroup> lookupGroups = dao.findAll(LookupGroup.class);
    	pageIndex = (pageIndex!=0)?pageIndex:0;
    	pageSize = (pageSize!=0)?pageSize:5;
    	pageNumber  = (pageNumber!=0)?pageNumber:1;
    	System.out.println("=========pageIndex====="+pageIndex);
    	System.out.println("=========pageSize======"+pageSize);
    	
    	Result result = Results.html();
    	result.render("listLookupGroup", dao.findByPage(LookupGroup.class, pageIndex, pageSize));
    	
    	//paging
    	/* * pageNumber -> The current page number
    	 * pageSize -> The number of items in each page
    	 * pagesAvailable -> The total number of pages*/
    	
    	HashMap<String, Object> paginationData = new HashMap<String, Object>();
    	paginationData.put("pagesAvailable", dao.findAll(LookupGroup.class).size());
    	paginationData.put("pageIndex",pageIndex);
    	paginationData.put("pageSize",pageSize);
    	paginationData.put("pageNumber",pageNumber);
    	result.render("paginationData", paginationData);
    	
    	System.out.println("=========pageIndex====="+pageIndex);
    	System.out.println("=========pageSize======"+pageSize);
    	System.out.println("=========TOTAL========="+dao.findAll(LookupGroup.class).size());
		return result;
    }
    public Result addLookupGroup(@Param("code") String code,@Param("description") String description, Context context, FlashScope flashScope) {
    		LookupGroup lookupGroup = new LookupGroup(code, description);
    		System.out.println("=========================================start==============================================");
    		List<LookupGroup> checkAvailbleData = dao.findByProperty(LookupGroup.class, "code", code);
    		if(checkAvailbleData.size()==0){//save
    			dao.save(lookupGroup);
    			context.getFlashScope().success("successfully.added");
    		}else//fail
    			context.getFlashScope().error("failed");
    		System.out.println("=========================================success==============================================");
			return Results.redirect("/admin/lookupGroupList");
	}
    public Result updateLookupGroup(@Param("id") Long id,@Param("code") String code,@Param("description") String description, Context context, FlashScope flashScope) {
		LookupGroup dataFromTable = dao.find(LookupGroup.class, id);
		System.out.println("=========================================start==============================================");
		List<LookupGroup> checkAvailbleData = dao.findByProperty(LookupGroup.class, "code", code);
		if(checkAvailbleData.size()==0||
				checkAvailbleData.size()>0 && checkAvailbleData.get(0).getCode().equals(code) && checkAvailbleData.get(0).getId() == id){//save
			LookupGroup update =  checkAvailbleData.get(0);
			update.setCode(code);
			update.setDescription(description);
	        dao.merge(update);
			context.getFlashScope().success("successfully.edited");
		}else//fail
			context.getFlashScope().error("failed");
		System.out.println("=========================================success==============================================");
		return Results.redirect("/admin/lookupGroupList");
    }
    public Result deleteLookupGroup(@Params("id") String[] ids,Context context, FlashScope flashScope) {
    	for(String id : ids){
    		//delete data on lookup table
    		List< Lookup> lookupList = dao.findByProperty(Lookup.class, "lookupgroup_id", Long.parseLong(id));
    		for(Lookup data :lookupList){
    			dao.delete(Lookup.class, data.getId());
    		}
    		
    		//delete data on lookup group table
    		dao.delete(LookupGroup.class, Long.parseLong(id));
    		context.getFlashScope().success("successfully.deleted");
    	}
    	return Results.redirect("/admin/lookupGroupList");
    }
    
    
    public Result lookupList() {
    	//List<LookupGroup> lookupGroups = dao.findAll(LookupGroup.class);
    	System.out.println("==========aa=====");
		return Results.html().render("listLookup", dao.findAll(Lookup.class, EnumOrder.ASC, "code"));
    }
    public Result deleteLookup(@Params("id") String[] ids,Context context, FlashScope flashScope) {
    	for(String id : ids){
    		//delete data on lookup  table
    		List<Lookup> checkAvailbleDataLookup = dao.findByProperty(Lookup.class, "id", id);
    		List<LookupGroup> checkAvailbleDataLookupGroup =  dao.findByProperty(LookupGroup.class, "code", checkAvailbleDataLookup.get(0).getCode());
    		System.out.println("===================="+Long.parseLong(id)+"=====================DeLeTe==============================================");
    		dao.delete(Lookup.class, Long.parseLong(id));
    		context.getFlashScope().success("successfully.deleted");
    	}
    	return Results.redirect("/admin/lookupList");
    }
    public Result addLookup(@Param("code") String code,@Param("name") String name,@Param("description") String description, Context context, FlashScope flashScope){
		System.out.println("=========================================start==============================================");
		List<Lookup> checkAvailbleDataLookup = dao.findByProperty(Lookup.class, "code", code);
		
		boolean allow=true;
		long id =0;
		for(Lookup data : checkAvailbleDataLookup){
			if(data.getName().equals(name)){
				allow=false;
			}
		}
		if(allow){
			LookupGroup data = dao.findByProperty(LookupGroup.class, "code", code).get(0); 
			Lookup lookup = new Lookup(data.getId(), code, name, description,"Y");
			lookup.setLookupGroup(data);
			dao.save(lookup);
			context.getFlashScope().success("successfully.added");
		}else//fail
			context.getFlashScope().error("failed");
		System.out.println("=========================================success==============================================");
    	
    	return Results.redirect("/admin/lookupList");
    }
    public Result updateLookup(@Param("id") Long id,@Param("code") String code,@Param("name") String name,@Param("description") String description,@Param("useYN") String useYN, Context context, FlashScope flashScope) {
		Lookup dataFromTable = dao.find(Lookup.class, id);
		System.out.println("=========================================start==============================================");
		List<Lookup> checkAvailbleData = dao.findByProperty(Lookup.class, "name", name);
		if(checkAvailbleData.size()==0||
				checkAvailbleData.size()>0 && checkAvailbleData.get(0).getName().equals(name) && checkAvailbleData.get(0).getId() == id){//save
			Lookup update = dataFromTable;
			update.setCode(code);
			update.setName(name);
			update.setDescription(description);
			update.setUse_yn(useYN);
	        dao.merge(update);
			context.getFlashScope().success("successfully.edited");
		}else//fail
			context.getFlashScope().error("failed");
		System.out.println("=========================================success==============================================");
		return Results.redirect("/admin/lookupList");
    }
    
    
    /*public Result checkAvailableLookupGroup(@Param("code") String code) {
        return Results.json().render(code);
    }
    */
    
    public Result categoryList() {
    	return Results.html();
    }
}

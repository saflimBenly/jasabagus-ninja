/**
 * Copyright (C) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import models.MailObj;
import models.Member;
import models.MemberSocmed;
import models.NewsLetter;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.params.Param;
import ninja.params.PathParam;
import ninja.session.FlashScope;
import ninja.session.Session;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.sun.org.apache.xpath.internal.patterns.ContextMatchStepPattern;

import dao.GenericDao;
import dao.MemberDao;
import dto.MemberDto;
import dto.SNSConnectDto;
import encrypt.EncryptUtils;

@Singleton
public class LoginLogoutController {
    
	private Log log = LogFactory.getLog(LoginLogoutController.class);
	
    @Inject
    MemberDao userDao;
    
    @Inject
	GenericDao dao;
    
    @Inject
    MailController mailController;
    
    @Inject 
    Provider<HttpServletRequest> request; 

    ///////////////////////////////////////////////////////////////////////////
    // Login
    ///////////////////////////////////////////////////////////////////////////
    public Result login(Context context, FlashScope flashScope) {
    	log.info(">>>>>>>>>>>>" + context.getFlashScope().get("error"));
        return Results.html();
    }

    public Result loginPost(@Param("username") String username,
                            @Param("password") String password,
                            Context context,
                            FlashScope flashScope) {
    	log.info(">>>>>> LOGIN CHECK <<<<<<<<<<");

//        boolean isUserNameAndPasswordValid  = userDao.isUserAndPasswordValid(username, password);
//	
//        Member member  = userDao.getkMemberDetail(username, password);
//		boolean a = false;
//        try {
//			a = PasswordHash.validatePassword(password, "1000:5cb43abe6dffc96b3d21ef890bd41194af58a4dfc4950d4e:675cfe4bd611c4dd7400a7d244efaab5af6049300febd9d6");
//		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
      
    	Member member = dao.findOneByProperty(Member.class, "username", username);
    	
//        if (isUserNameAndPasswordValid) {
    	  if (member!=null){
    		  log.info(">>>>>> LOGIN SUCCESS <<<<<<<<<<");
            context.getSession().put("username", username);
            context.getSession().put("memNo", member.getId().toString());
            context.getSession().put("name", (member.getFullname()==null)?member.getFirstname()+" "+member.getLastname():member.getFullname());
            context.getFlashScope().success("login.loginSuccessful");
            //flashScope.success("login.loginSuccessful");
            
            return Results.redirect("/");
            
        } else {
        	log.info(">>>>>> LOGIN FAILED <<<<<<<<<<");
            // something is wrong with the input or password not found.
        	context.getFlashScope().error("login.errorLogin");
        	//flashScope.error("login.errorLogin");

            return Results.redirect("/login");
        }
        
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Logout
    ///////////////////////////////////////////////////////////////////////////
    public Result logout(Context context) {

        // remove any user dependent information
        context.getSession().clear();
        context.getFlashScope().success("login.logoutSuccessful");

        return Results.redirect("/");

    }
    
    public Result loginSNS(SNSConnectDto snsConnectDto,
			Context context,
			FlashScope flashScope){
    	
    	SNSReturnData snsReturnData = handleSNS("login", snsConnectDto, context, flashScope);
    	
    	if (snsReturnData.errorMsg!=null && snsReturnData.errorMsg.length() > 0) context.getFlashScope().error(snsReturnData.errorMsg);
    	if (snsReturnData.successMsg!=null && snsReturnData.successMsg.length() > 0) context.getFlashScope().success(snsReturnData.successMsg);
    	
    	return Results.json().render(snsReturnData); 
    }
    
    public Result signupSNS(SNSConnectDto snsConnectDto,
			Context context,
			FlashScope flashScope){

    	SNSReturnData snsReturnData = handleSNS("signup", snsConnectDto, context, flashScope);

    	if (snsReturnData.errorMsg!=null && snsReturnData.errorMsg.length() > 0) context.getFlashScope().error(snsReturnData.errorMsg);
    	if (snsReturnData.successMsg!=null && snsReturnData.successMsg.length() > 0) context.getFlashScope().success(snsReturnData.successMsg);

		return Results.json().render(snsReturnData); 
    }
    
    
    ///////////////////////////////////////////////////////////////////////////
    // Login and SignUp from Auth0 for social network, Google + Twitter
    ///////////////////////////////////////////////////////////////////////////
	public SNSReturnData handleSNS(String state, 
						SNSConnectDto snsConnectDto,
						Context context,
						FlashScope flashScope) {
		 
		SNSReturnData snsReturnData = new SNSReturnData();
		String snsname = "";
		String snsId = "";
		boolean successFlag = false;
		Member member = new Member();
		
		log.info(">>> FB/Google/Twitter LOGIN <<<");
//		Member member = userDao.isUserNameAndFbIdValid(fbLoginDto);
		
		if (snsConnectDto.getUser_id() != null) {
			if(snsConnectDto.getUser_id().length() > 0) { //from Auth0
				String[] userDesc = snsConnectDto.getUser_id().split("\\|");
				snsname = userDesc[0].equals("twitter")? "Twitter":"Google";
				snsId = userDesc[1];
			} else {
				snsname = "Facebook";
				snsId = snsConnectDto.getId();
			}
		}
		
		//find member, based on snsId
		MemberSocmed socmed = dao.findOneByProperty(MemberSocmed.class, "socmedno", snsId);
        
		//if found, then success
		if(socmed!=null) {
			member = dao.find(Member.class, socmed.getMember().getId());
			setUserSession(context.getSession(), member);
			snsReturnData.successMsg = "login.loginSuccessful";
	        successFlag = true;
	        
	        //if not found
		} else {
			//sign-up then register to db
			if (state.equals("signup")) {
				//prepare the data
				member = dao.findOneByProperty(Member.class, "username", snsConnectDto.getEmail());

				//add new socmed to member
				if(member!=null) {
					socmed = new MemberSocmed();
					socmed.setMember(member);
					socmed.setSocmedno(snsId);
					socmed.setResfield1(snsname);
					socmed.setResfield2(snsConnectDto.getName());
					socmed.setResfield3(snsConnectDto.getPicture());

					dao.save(socmed);
					setUserSession(context.getSession(), member);
				} else {
					//register to db
					member = new Member();
					member.setUsername(snsConnectDto.getEmail());
					member.setFullname(snsConnectDto.getName());
					member.setFirstname(snsConnectDto.getGiven_name());
					member.setLastname(snsConnectDto.getFamily_name());
					member.setGender(snsConnectDto.getGender());
					member.setStatus("02");
					
					Set<MemberSocmed> socmeds = new HashSet<MemberSocmed>();
					socmed = new MemberSocmed();
					socmed.setMember(member);
					socmed.setSocmedno(snsId);
					socmed.setResfield1(snsname);
					socmed.setResfield2(snsConnectDto.getName());
					socmed.setResfield3(snsConnectDto.getPicture());
					socmeds.add(socmed);

					member.setSocmed(socmeds);
					
					dao.save(member);
					setUserSession(context.getSession(), member);
				}
				snsReturnData.successMsg = "signup.signUpSocial.success";
		        successFlag = true;

			//login not found, redirect to sign-up	
			} else if (state.equals("login")) {
				snsReturnData.errorMsg = "login.errorLogin.register.sns";
	        	successFlag = false;
			}
        	
        } //endif socmed exist
        
		snsReturnData.snsname = snsname;
		snsReturnData.result = (successFlag?"success.sns":"not.register.sns");

		return snsReturnData; 
	}    

    //////////////////////////////////////////////////////////////////////////////////////
    // Sign Up
    /////////////////////////////////////////////////////////////////////////////////////
    public Result signUp(Context context, FlashScope flashScope) {
        return Results.html().render("account", "already");
    }
    
    public Result signUpPost(
            Context context,
            FlashScope flashScope,
            MemberDto memberDto) {
    	try{
	    	//boolean isUserNameAndPasswordValid = userDao.isUserAndPasswordValid(memberDto.username, memberDto.password);
//	    	Member member  = userDao.getkMemberDetail(memberDto.username, memberDto.password);
	    	Member member  = dao.findOneByProperty(Member.class, "username", memberDto.username);
	        
	    	if(member!=null){
	    		context.getFlashScope().error("signup.email.already");
	        	return Results.redirect("/sign-up");
	        }
    		
    		Boolean result = userDao.isRegisterSuccess(memberDto);
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}	
    	//return Results.redirect("/");
    	return Results.html();
    }

    ///////////////////////////////////////////////////////////////////////////
    // Sign-Up From FB
    ///////////////////////////////////////////////////////////////////////////
    public Result signUpFB(@Param("first_name") String firstName,
    						@Param("last_name") String lastName,
    						@Param("id") String fbId,
    						@Param("email") String email,
    						Context context,
    						FlashScope flashScope) throws MalformedURLException, IOException {
    
    	log.info(">>> FB REGISTRATION <<<");
    	
    	//boolean =isUserNameAndFbIdValid  false;
    	
    	SNSReturnData snsReturnData = new SNSReturnData();
    	
    	SNSConnectDto snsConnectDto = new SNSConnectDto();
    	snsConnectDto.setFirst_name(firstName);
    	snsConnectDto.setLast_name(lastName);
    	snsConnectDto.setId(fbId);
    	snsConnectDto.setEmail(email);
    	snsConnectDto.setStatus("02");
		
		log.info(">>> CHECK EXISTING FB ACCOUNT <<<");
		
		Member member = dao.findOneByProperty(Member.class, "username", snsConnectDto.getEmail());
        if(member!=null){
        	snsReturnData.result = "already";
            // something is wrong with the input or password not found.
        	context.getFlashScope().error("signup.email.already");
        	//flashScope.error("login.errorLogin");
        	return Results.json().render(snsReturnData);
        }
        
    	boolean isSuccessRegister = userDao.isRegisterSMSuccess(snsConnectDto);
    	
    	log.info("result="+isSuccessRegister);
    	
		if(isSuccessRegister){
	        member = dao.findOneByProperty(Member.class, "username", snsConnectDto.getEmail());
	        
//	       String a  = downloadFacebookProfilePicture(member.getFbid(), "CAAT0R92UgGwBAHY4Om3ZByXbWqWXAMt8qa7ym0Xnd8VS7o7vBogCvPjmvIqcsGdsLgm6yiG8OB2NSgMm9WktRZAyZCdFO77nsb4DhrfJuv9TBri20PKmj8ewZAq1ZAU9YqbZAfHBSoRCtCzWR9yXorA3cIwMuBL9LdryozwZBMFfQjnbF1FOBoC6LeMQVRWoe1RcwvYIISX9QZDZD", "Septian");
//	       log.info(a);
		}
		
		if (member!=null) {

            context.getSession().put("username", snsConnectDto.getId());
            context.getFlashScope().success("login.loginSuccessful");
           
	     //  fbReturnData.result = (member!=null?"success.fb":"not.register.fb");
	        
	        context.getSession().put("username", snsConnectDto.getEmail());
	        context.getSession().put("memNo", member.getId().toString());
            context.getSession().put("name", (member.getFullname()==null)?member.getFirstname()+" "+member.getLastname():member.getFullname());
	        context.getFlashScope().success("login.loginSuccessful");

        } else {
            // something is wrong with the input or password not found.
        	context.getFlashScope().error("login.errorLogin");
        	//flashScope.error("login.errorLogin");
	
        }
		return Results.json().render(snsReturnData);
	}    
    
    ///////////////////////////////////////////////////////////////////////////
    // Verification Account 
    ///////////////////////////////////////////////////////////////////////////
    public Result verificationAccount(@PathParam("key") String key,
    		Context context,
			FlashScope flashScope) throws MalformedURLException, IOException{
    	
    	String decrypt = EncryptUtils.base64decode(key);
    	
    	Long id = Long.parseLong(decrypt.split("-")[0]);
    	
    	Member member = dao.find(Member.class, id);
    	
    	if(member.getStatus().equals("01")){
    		member.setStatus("02");
    		dao.merge(member);
    		context.getFlashScope().success("signup.verified.success");
    	}else{
            // something is wrong with the input or password not found.
        	context.getFlashScope().error("signup.verified.already");
        	//flashScope.error("login.errorLogin");
    	}
    	
    	return Results.redirect("/admin/memberDetail/"+id);
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Join Newsletter
    ///////////////////////////////////////////////////////////////////////////
    public Result joinNewsLetter(
			@Param("email") String email,
			Context context,
			FlashScope flashScope) throws MalformedURLException, IOException {

		NewsLetter newsLetter =  dao.findOneByProperty(NewsLetter.class, "email", email);

		Boolean result = false;
		
		if(newsLetter==null){
			newsLetter = new NewsLetter();
			newsLetter.setEmail(email);
			Calendar c = Calendar.getInstance();
			newsLetter.setCreatedt(c);
			dao.save(newsLetter);
			
			String subject = "Newsletter to Keep You Update";
	        String[] addTo = {newsLetter.getEmail()};
	        
	        String content = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><meta name='viewport' content='width=device-width'/><style>#outlook a{padding:0}body{width:100% !important;min-width:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0}.ExternalClass{width:100%}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%}#backgroundTable{margin:0;padding:0;width:100% !important;line-height:100% !important}img{outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:auto;max-width:100%;float:left;clear:both;display:block}center{width:100%;min-width:580px}a img{border:none}p{margin:0 0 0 10px}table{border-spacing:0;border-collapse:collapse}td{word-break:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important}table,tr,td{padding:0;vertical-align:top;text-align:left}hr{color:#d9d9d9;background-color:#d9d9d9;height:1px;border:none}table.body{height:100%;width:100%}table.container{width:580px;margin:0 auto;text-align:inherit}table.row{padding:0px;width:100%;position:relative}table.container table.row{display:block}td.wrapper{padding:10px 20px 0px 0px;position:relative}table.columns,table.column{margin:0 auto}table.columns td, table.column td{padding:0px 0px 10px}table.columns td.sub-columns, table.column td.sub-columns, table.columns td.sub-column, table.column td.sub-column{padding-right:10px}td.sub-column,td.sub-columns{min-width:0px}table.row td.last, table.container td.last{padding-right:0px}table.one{width:30px}table.two{width:80px}table.three{width:130px}table.four{width:180px}table.five{width:230px}table.six{width:280px}table.seven{width:330px}table.eight{width:380px}table.nine{width:430px}table.ten{width:480px}table.eleven{width:530px}table.twelve{width:580px}table.one center{min-width:30px}table.two center{min-width:80px}table.three center{min-width:130px}table.four center{min-width:180px}table.five center{min-width:230px}table.six center{min-width:280px}table.seven center{min-width:330px}table.eight center{min-width:380px}table.nine center{min-width:430px}table.ten center{min-width:480px}table.eleven center{min-width:530px}table.twelve center{min-width:580px}table.one .panel center{min-width:10px}table.two .panel center{min-width:60px}table.three .panel center{min-width:110px}table.four .panel center{min-width:160px}table.five .panel center{min-width:210px}table.six .panel center{min-width:260px}table.seven .panel center{min-width:310px}table.eight .panel center{min-width:360px}table.nine .panel center{min-width:410px}table.ten .panel center{min-width:460px}table.eleven .panel center{min-width:510px}table.twelve .panel center{min-width:560px}.body .columns td.one, .body .column td.one{width:8.333333%}.body .columns td.two, .body .column td.two{width:16.666666%}.body .columns td.three, .body .column td.three{width:25%}.body .columns td.four, .body .column td.four{width:33.333333%}.body .columns td.five, .body .column td.five{width:41.666666%}.body .columns td.six, .body .column td.six{width:50%}.body .columns td.seven, .body .column td.seven{width:58.333333%}.body .columns td.eight, .body .column td.eight{width:66.666666%}.body .columns td.nine, .body .column td.nine{width:75%}.body .columns td.ten, .body .column td.ten{width:83.333333%}.body .columns td.eleven, .body .column td.eleven{width:91.666666%}.body .columns td.twelve, .body .column td.twelve{width:100%}td.offset-by-one{padding-left:50px}td.offset-by-two{padding-left:100px}td.offset-by-three{padding-left:150px}td.offset-by-four{padding-left:200px}td.offset-by-five{padding-left:250px}td.offset-by-six{padding-left:300px}td.offset-by-seven{padding-left:350px}td.offset-by-eight{padding-left:400px}td.offset-by-nine{padding-left:450px}td.offset-by-ten{padding-left:500px}td.offset-by-eleven{padding-left:550px}td.expander{visibility:hidden;width:0px;padding:0 !important}table.columns .text-pad, table.column .text-pad{padding-left:10px;padding-right:10px}table.columns .left-text-pad, table.columns .text-pad-left, table.column .left-text-pad, table.column .text-pad-left{padding-left:10px}table.columns .right-text-pad, table.columns .text-pad-right, table.column .right-text-pad, table.column .text-pad-right{padding-right:10px}.block-grid{width:100%;max-width:580px}.block-grid td{display:inline-block;padding:10px}.two-up td{width:270px}.three-up td{width:173px}.four-up td{width:125px}.five-up td{width:96px}.six-up td{width:76px}.seven-up td{width:62px}.eight-up td{width:52px}table.center,td.center{text-align:center}h1.center,h2.center,h3.center,h4.center,h5.center,h6.center{text-align:center}span.center{display:block;width:100%;text-align:center}img.center{margin:0 auto;float:none}.show-for-small,.hide-for-desktop{display:none}body,table.body,h1,h2,h3,h4,h5,h6,p,td{color:#222;font-family:'Helvetica','Arial',sans-serif;font-weight:normal;padding:0;margin:0;text-align:left;line-height:1.3}h1,h2,h3,h4,h5,h6{word-break:normal}h1{font-size:40px}h2{font-size:36px}h3{font-size:32px}h4{font-size:28px}h5{font-size:24px}h6{font-size:20px}body,table.body,p,td{font-size:14px;line-height:19px}p.lead,p.lede,p.leed{font-size:18px;line-height:21px}p{margin-bottom:10px}small{font-size:10px}a{color:#2ba6cb;text-decoration:none}a:hover{color:#2795b6 !important}a:active{color:#2795b6 !important}a:visited{color:#2ba6cb !important}h1 a, h2 a, h3 a, h4 a, h5 a, h6 a{color:#2ba6cb}h1 a:active, h2 a:active, h3 a:active, h4 a:active, h5 a:active, h6 a:active{color:#2ba6cb !important}h1 a:visited, h2 a:visited, h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited{color:#2ba6cb !important}.panel{background:#f2f2f2;border:1px solid #d9d9d9;padding:10px !important}.sub-grid table{width:100%}.sub-grid td.sub-columns{padding-bottom:0}table.button,table.tiny-button,table.small-button,table.medium-button,table.large-button{width:100%;overflow:hidden}table.button td, table.tiny-button td, table.small-button td, table.medium-button td, table.large-button td{display:block;width:auto !important;text-align:center;background:#2ba6cb;border:1px solid #2284a1;color:#fff;padding:8px 0}table.tiny-button td{padding:5px 0 4px}table.small-button td{padding:8px 0 7px}table.medium-button td{padding:12px 0 10px}table.large-button td{padding:21px 0 18px}table.button td a, table.tiny-button td a, table.small-button td a, table.medium-button td a, table.large-button td a{font-weight:bold;text-decoration:none;font-family:Helvetica,Arial,sans-serif;color:#fff;font-size:16px}table.tiny-button td a{font-size:12px;font-weight:normal}table.small-button td a{font-size:16px}table.medium-button td a{font-size:20px}table.large-button td a{font-size:24px}table.button:hover td, table.button:visited td, table.button:active td{background:#2795b6 !important}table.button:hover td a, table.button:visited td a, table.button:active td a{color:#fff !important}table.button:hover td, table.tiny-button:hover td, table.small-button:hover td, table.medium-button:hover td, table.large-button:hover td{background:#2795b6 !important}table.button:hover td a, table.button:active td a, table.button td a:visited, table.tiny-button:hover td a, table.tiny-button:active td a, table.tiny-button td a:visited, table.small-button:hover td a, table.small-button:active td a, table.small-button td a:visited, table.medium-button:hover td a, table.medium-button:active td a, table.medium-button td a:visited, table.large-button:hover td a, table.large-button:active td a, table.large-button td a:visited{color:#fff !important}table.secondary td{background:#e9e9e9;border-color:#d0d0d0;color:#555}table.secondary td a{color:#555}table.secondary:hover td{background:#d0d0d0 !important;color:#555}table.secondary:hover td a, table.secondary td a:visited, table.secondary:active td a{color:#555 !important}table.success td{background:#5da423;border-color:#457a1a}table.success:hover td{background:#457a1a !important}table.alert td{background:#c60f13;border-color:#970b0e}table.alert:hover td{background:#970b0e !important}table.radius td{-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px}table.round td{-webkit-border-radius:500px;-moz-border-radius:500px;border-radius:500px}body.outlook p{display:inline !important}@media only screen and (max-width: 600px){table[class='body'] img{width:auto !important;height:auto !important}table[class='body'] center{min-width:0 !important}table[class='body'] .container{width:95% !important}table[class='body'] .row{width:100% !important;display:block !important}table[class='body'] .wrapper{display:block !important;padding-right:0 !important}table[class='body'] .columns, table[class='body'] .column{table-layout:fixed !important;float:none !important;width:100% !important;padding-right:0px !important;padding-left:0px !important;display:block !important}table[class='body'] .wrapper.first .columns, table[class='body'] .wrapper.first .column{display:table !important}table[class='body'] table.columns td, table[class='body'] table.column td{width:100% !important}table[class='body'] .columns td.one, table[class='body'] .column td.one{width:8.333333% !important}table[class='body'] .columns td.two, table[class='body'] .column td.two{width:16.666666% !important}table[class='body'] .columns td.three, table[class='body'] .column td.three{width:25% !important}table[class='body'] .columns td.four, table[class='body'] .column td.four{width:33.333333% !important}table[class='body'] .columns td.five, table[class='body'] .column td.five{width:41.666666% !important}table[class='body'] .columns td.six, table[class='body'] .column td.six{width:50% !important}table[class='body'] .columns td.seven, table[class='body'] .column td.seven{width:58.333333% !important}table[class='body'] .columns td.eight, table[class='body'] .column td.eight{width:66.666666% !important}table[class='body'] .columns td.nine, table[class='body'] .column td.nine{width:75% !important}table[class='body'] .columns td.ten, table[class='body'] .column td.ten{width:83.333333% !important}table[class='body'] .columns td.eleven, table[class='body'] .column td.eleven{width:91.666666% !important}table[class='body'] .columns td.twelve, table[class='body'] .column td.twelve{width:100% !important}table[class='body'] td.offset-by-one, table[class='body'] td.offset-by-two, table[class='body'] td.offset-by-three, table[class='body'] td.offset-by-four, table[class='body'] td.offset-by-five, table[class='body'] td.offset-by-six, table[class='body'] td.offset-by-seven, table[class='body'] td.offset-by-eight, table[class='body'] td.offset-by-nine, table[class='body'] td.offset-by-ten, table[class='body'] td.offset-by-eleven{padding-left:0 !important}table[class='body'] table.columns td.expander{width:1px !important}table[class='body'] .right-text-pad, table[class='body'] .text-pad-right{padding-left:10px !important}table[class='body'] .left-text-pad, table[class='body'] .text-pad-left{padding-right:10px !important}table[class='body'] .hide-for-small, table[class='body'] .show-for-desktop{display:none !important}table[class='body'] .show-for-small, table[class='body'] .hide-for-desktop{display:inherit !important}}</style><style>table.facebook td{background:#3b5998;border-color:#2d4473}table.facebook:hover td{background:#2d4473 !important}table.twitter td{background:#00acee;border-color:#0087bb}table.twitter:hover td{background:#0087bb !important}table.google-plus td{background-color:#DB4A39;border-color:#C00}table.google-plus:hover td{background:#C00 !important}.template-label{color:#fff;font-weight:bold;font-size:11px}.callout .wrapper{padding-bottom:20px}.callout .panel{background:#ECF8FF;border-color:#b9e5ff}.header{background:#999}.footer .wrapper{background:#ebebeb}.footer h5{padding-bottom:10px}table.columns .text-pad{padding-left:10px;padding-right:10px}table.columns .left-text-pad{padding-left:10px}table.columns .right-text-pad{padding-right:10px}@media only screen and (max-width: 600px){table[class='body'] .right-text-pad{padding-left:10px !important}table[class='body'] .left-text-pad{padding-right:10px !important}}</style></head><body><table class='body'><tr><td class='center' align='center' valign='top'><center><table class='row header'><tr><td class='center' align='center'><center><table class='container'><tr><td class='wrapper last'><table class='twelve columns'><tr><td class='six sub-columns'> <img src='http://zebravan.co/assets/img/logo-small.png'></td><td class='six sub-columns last' style='text-align:right; vertical-align:middle;'> <span class='template-label'>Zebravan.co</span></td><td class='expander'></td></tr></table></td></tr></table></center></td></tr></table><table class='container'><tr><td><table class='row'><tr><td class='wrapper last'><table class='twelve columns'><tr><td><h1>Hi Jasgusers,</h1><p class='lead'>Terima kasih telah mendaftarkan email Anda.</p><p>Nantikan berita-berita terbaru dari kami, untuk memudahkan Anda mencari jasa yang profesional, terpercaya dan bersahabat. Komitmen kami adalah memberikan layanan terbaik bagi kemajuan dunia usaha jasa di Indonesia.</p></td><td class='expander'></td></tr></table></td></tr></table><table class='row callout'><tr><td class='wrapper last'><table class='twelve columns'><tr><td class='panel'><p>Kembali ke halaman utama kami. <a href='http://zebravan.co/landing'>Click it! �</a></p></td><td class='expander'></td></tr></table></td></tr></table><table class='row footer'><tr><td class='wrapper'><table class='six columns'><tr><td class='left-text-pad'><h5>Connect With Us:</h5><table class='tiny-button facebook'><tr><td> <a href='#'>Facebook</a></td></tr></table><br><table class='tiny-button twitter'><tr><td> <a href='#'>Twitter</a></td></tr></table><br><table class='tiny-button google-plus'><tr><td> <a href='#'>Google +</a></td></tr></table></td><td class='expander'></td></tr></table></td><td class='wrapper last'><table class='six columns'><tr><td class='last right-text-pad'><h5>Contact Info:</h5><p>Phone: 0816 1179846</p><p>Email: <a href='mailto:hseldon@trantor.com'>admin@zebravan.co</a></p></td><td class='expander'></td></tr></table></td></tr></table><table class='row'><tr><td class='wrapper last'><table class='twelve columns'><tr><td align='center'><center><p style='text-align:center;'><a href='http://www.zebravan.co/terms'>Terms</a> | <a href='http://www.zebravan.co/policy'>Privacy</a> | <a href='http://wwww.zebravan.co/unsubscibe'>Unsubscribe</a></p></center></td><td class='expander'></td></tr></table></td></tr></table></td></tr></table></center></td></tr></table></body></html>";
	        
	        MailObj mailObj = new MailObj(subject, "admin@zebravan.co", addTo, content, "");
	         
	        mailController.sendMail(mailObj);
	         
			result = true;
		} else {
        	context.getFlashScope().error("signup.verified.already");
		}
		
		return Results.json().render(result);
}    

   
    /* ========================
     *  Callback from Auth0
     *  using code 
     * example : callback?code=tc1iobJZD3sKd4HM&state=oooFLoNEbjMwPa7G#
     * using access token 
     * ex : #access_token=8uidgjbIQVxixfwp&token_type=Bearer&state=XdG1oUt1zaxk8zLT
     * ===========================
     */
	public Result callback(@PathParam("state") String state, 
							Context context,
							FlashScope flashScope, Object myObject) {
    	SNSReturnData snsReturnData = new SNSReturnData();
    	snsReturnData.result = state;
    	//Map<String, String[]> list = context.getParameters();
    	//snsReturnData.result = list.get("access_token")[0].toString();
    	return Results.html();
	}

	private void setUserSession(Session session, Member member) {
		session.put("username", member.getUsername());
		session.put("memNo", member.getId().toString());
		session.put("name", (member.getFullname()==null)?member.getFirstname()+" "+member.getLastname():member.getFullname());
	}

	public static class SNSReturnData {
        public String result;
        public String snsname;
        public String errorMsg;
        public String successMsg;
    }

	
//    private static final String PROFILE_PICTURE_URL = "https://graph.facebook.com/%s/picture?type=large&access_token=%s";

//    private String downloadFacebookProfilePicture(String uid, String accessToken, String username) throws MalformedURLException, IOException {
//    	//Get the picture url
//    	HttpURLConnection.setFollowRedirects(false);
//    	String profilePictureUrlString = new URL(String.format(PROFILE_PICTURE_URL, uid, accessToken)).openConnection().getHeaderField(
//    			"location");
//
//    	//Set up temp location for picture
//    	String fileExtension = StringUtils.substring(profilePictureUrlString,
//    			StringUtils.lastIndexOf(profilePictureUrlString, ".") + 1);
//    	
//    	String tempFilePath = (new StringBuilder(String.valueOf(System
//    			.getProperty("java.io.tmpdir"))))
//    			.append(StringUtils.replace(username, ".", "_")).append(".")
//    			.append(fileExtension).toString();
//
//    	String exportUrl = profilePictureUrlString;
//    		//Download file to temp  location
//    		downloadFile(exportUrl, tempFilePath);
//
//    	return tempFilePath;
//    }    	
    
//    private void downloadFile(String exportUrl, String filepath)
//    		throws IOException, MalformedURLException {
//    	InputStream inStream = new URL(exportUrl).openStream();
//    	FileOutputStream outStream = new FileOutputStream(filepath);
//    	copyStreams(inStream, outStream);
//    }
    
//    private void copyStreams(InputStream inStream, OutputStream outStream)
//    		throws IOException {
//    	try {
//    		int c;
//    		while ((c = inStream.read()) != -1) {
//    			outStream.write(c);
//    		}
//    	} finally {
//    		if (inStream != null) {
//    			inStream.close();
//    		}
//    		if (outStream != null) {
//    			outStream.flush();
//    			outStream.close();
//    		}
//    	}
//    }
}

-- Alter Lookup
ALTER TABLE Lookup
ADD COLUMN code varchar(10),
ADD COLUMN name varchar(100),
DROP COLUMN lookupcode,
DROP COLUMN lookupname
;

ALTER TABLE Lookup
ALTER COLUMN code SET NOT NULL;

-- member add Profile
ALTER TABLE Member
ADD COLUMN profile VARCHAR(150);

-- Address
CREATE TABLE Address (
  id bigserial primary key,
  addresstype VARCHAR(20),
  addressline1 VARCHAR(200),
  addressline2 VARCHAR(200),
  district VARCHAR(20),
  city VARCHAR(20),
  province VARCHAR(20),
  country VARCHAR(20),
  postcode VARCHAR(10),
  member_id BIGSERIAL references Member(id)
);

-- MainBillboard
CREATE TABLE MainBillboard (
  id bigserial primary key,
  startdate DATE,
  enddate DATE,
  name VARCHAR(100),
  description VARCHAR(100),
  url VARCHAR(100)
);


-- Category
CREATE TABLE Category (
  id bigserial primary key,
  code VARCHAR(10) NOT NULL,
  description VARCHAR(200),
  parent BIGSERIAL,
  level INT
);

-- ServiceProvider
CREATE TABLE ServiceProvider (
  id bigserial primary key,
  name VARCHAR(100),
  storename VARCHAR(100),
  description VARCHAR(200),
  member_id BIGSERIAL REFERENCES Member(id)
);

-- ServiceProduct
CREATE TABLE ServiceProduct (
  id bigserial primary key,
  name VARCHAR(100),
  code VARCHAR(20),
  url VARCHAR(100),
  price INTEGER,
  pricetype VARCHAR(20),
  description VARCHAR(200),
  serviceprovider_id BIGSERIAL REFERENCES ServiceProvider(id),
  category_id BIGSERIAL REFERENCES Category(id)
);

-- Carousel
CREATE TABLE Carousel (
  id bigserial primary key,
  title VARCHAR(100) NOT NULL,
  tagline VARCHAR(100),
  url VARCHAR(100),
  mainbillboard_id BIGSERIAL references MainBillboard(id),
  serviceproduct_id BIGSERIAL REFERENCES ServiceProduct(id)
);

-- Contact
CREATE TABLE Contact (
  id bigserial primary key,
  contacttype VARCHAR(20),
  detail VARCHAR(100),
  channel VARCHAR(20),
  member_id BIGSERIAL REFERENCES Member(id)
);

-- FrontStore
CREATE TABLE FrontStore (
  id bigserial primary key,
  title VARCHAR(100),
  description VARCHAR(200),
  formatstyle VARCHAR(20),
  slug VARCHAR(100),
  serviceprovider_id BIGSERIAL REFERENCES ServiceProvider(id)
);


-- MemberSocmed
CREATE TABLE MemberSocmed (
  id bigserial primary key,
  socmedno VARCHAR(50),
  resfield1 VARCHAR(50),
  resfield2 VARCHAR(50),
  resfield3 VARCHAR(50),
  resfield4 VARCHAR(50),
  member_id BIGSERIAL REFERENCES Member(id)
);


-- member status
ALTER TABLE Member
ADD COLUMN status VARCHAR(2);

--encrypt certificate
ALTER TABLE Member
ADD COLUMN certificate VARCHAR(255);
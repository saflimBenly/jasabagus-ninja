-- image bytea
ALTER TABLE Member 
	ADD COLUMN image BYTEA;

create table NewsLetter (
    id bigserial primary key,
    email varchar(255),
    createdt date
);

-- the first script for migration
create table Article (
    id bigserial primary key,
    content varchar(5000) NOT NULL,
    postedAt timestamp,
    title varchar(255) NOT NULL
);

create table Member (
    id bigserial primary key,
    fullname varchar(255),
    isAdmin boolean NOT NULL,
    password varchar(255),
    username varchar(255)
);

create table Article_authorIds (
    Article_id bigserial references Article (id),
    authorIds bigserial references Member (id)
);

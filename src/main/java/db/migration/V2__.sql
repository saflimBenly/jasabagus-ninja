-- adding Member table with salt, fb related field. LookupGroup, Lookup 

ALTER TABLE Member 
			ADD COLUMN salt character varying(50), 
			ADD COLUMN fbid character varying(35), 
			ADD COLUMN firstname varchar(100), 
			ADD COLUMN lastname varchar(100), 
			ADD COLUMN birthdate date, 
			ADD COLUMN gender character(2), 
			ADD COLUMN fblogin boolean
			;

create table LookupGroup (
    id bigserial primary key,
    code varchar(10) NOT NULL UNIQUE,
    description varchar(100)
);

create table Lookup (
    id bigserial primary key,
	lookupgroup_code varchar(10) references LookupGroup(code), 
	lookupcode varchar(10) NOT NULL,
	lookupname varchar(100)
);
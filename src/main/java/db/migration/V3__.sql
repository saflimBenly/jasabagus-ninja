ALTER TABLE Lookup
			ADD COLUMN description character varying(100),
      ADD COLUMN lookupgroup_id BIGSERIAL references LookupGroup(id),
      DROP COLUMN lookupgroup_code
			;
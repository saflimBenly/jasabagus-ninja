package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import models.Category;
import models.Lookup;
import models.LookupGroup;
import ninja.jpa.UnitOfWork;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

import dto.CategoryDto;
import dto.LookupDto;

public class CategoryDao extends GenericDao {
	
	private Log log = LogFactory.getLog(CategoryDao.class);    

	@UnitOfWork
    public List<Category> getAllCategory() {
        List<Category> categories = findAll(Category.class);
        return categories;
    }
	
	@UnitOfWork
    public Category getCategoryById(Long id) {
       Category category = find(Category.class, id);

        return category;
    }
	
	@UnitOfWork
    public Category getCategoryByCode(String code) {

        Category category = findOneByProperty(Category.class, "code", code);

        return category;
    }
	
	 /**
     * Returns false if category cannot be found in database.
     */
    @Transactional
    public boolean saveCategory(CategoryDto categoryDto) {
        
//        EntityManager entityManager = entityManagerProvider.get();
//
//        Query query = entityManager.createQuery("SELECT x FROM Category x WHERE code = :codeParam");
//        log.info(categoryDto.code);
//        List<Category>data = (List<Category>) query.setParameter("codeParam", categoryDto.code).getResultList();
//
//        log.info("data  :"+data.size() );
//        if (data == null) {
//            return false;
//        }
//
//        Category category = new Category();
       /* log.info("data.getId :"+data.getId());
        log.info("data.getCode :"+data.getCode());
        log.info("data.getDescription :"+data.getDescription());
        log.info("data.getLevel :"+data.getLevel());
        log.info("data.getParent :"+data.getParent());
        category.setId(data.getId());
        category.setCode(data.getCode());
        category.setDescription(data.getDescription());
        category.setLevel(data.getLevel());
        category.setParent(data.getParent());
        entityManager.persist(category);*/
        
        return true;
        
    }

}

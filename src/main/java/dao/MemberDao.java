/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dao;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import controllers.MailController;
import dto.SNSConnectDto;
import encrypt.EncryptUtils;
import etc.PasswordHash;
import models.EnumOrder;
import models.MailObj;
import models.Member;
import dto.MemberDto;
import ninja.jpa.UnitOfWork;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

public class MemberDao extends GenericDao {

    @Inject
    MailController mailController;

//	private MailObj mailObj;

    @UnitOfWork
    public boolean isUserAndPasswordValid(String username, String password) {

        if (username != null && password != null) {

            try {
                Query q = getEntityManager().createQuery("SELECT x FROM Member x WHERE username = :usernameParam AND fbid is null");
                Member member = (Member) q.setParameter("usernameParam", username).getSingleResult();

                if (member != null) {
//                    if (member.getPassword().equals(password)) {
//                        return true;
//                    }
                    if (PasswordHash.validatePassword(password, member.getPassword())) {
                        return true;
                    }
                }
            } catch (NoResultException e) {
                //nothing
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @UnitOfWork
    public Member getMemberDetail(String username, String password) {
    	   Member member = null;
        if (username != null && password != null) {

            try {
                Query q = getEntityManager().createQuery("SELECT x FROM Member x WHERE username = :usernameParam AND fbid is null");
                member = (Member) q.setParameter("usernameParam", username).getSingleResult();

//                if (member != null) {
////                    if (member.getPassword().equals(password)) {
////                        return true;
////                    }
//                    if (PasswordHash.validatePassword(password, member.getPassword())) {
//                        return true;
//                    }
//                }
            } catch (NoResultException e) {
                //nothing
            }
        }
        return member;
    }
    
    @UnitOfWork
    public Member isUserNameAndFbIdValid(SNSConnectDto snsConnectDto) {
        //  if (snsConnectDto.getId() != null && snsConnectDto.getEmail() != null) {
    	Member member = null;
        try {
            Query q = getEntityManager().createQuery("SELECT x FROM Member x WHERE x.fbid = :fbId");
            member = (Member) q.setParameter("fbId", snsConnectDto.getId()).getSingleResult();

            if (member != null) {
                if (member.getFbid().equals(snsConnectDto.getId())) {
                    return member;
                }
            }
        } catch (NoResultException e) {
            e.printStackTrace();
        }
        //  }
        return member;
    }

    //List All Member
    @UnitOfWork
    public List<Member> getAllMember() {
        //Query q = getEntityManager().createQuery("SELECT x FROM Member x ORDER BY x.fullname ASC");
        //List<Member> members = (List<Member>) q.setFirstResult(1).setMaxResults(10).getResultList();
        // return q.setMaxResults(10).getResultList();
        List<Member> members = findAll(Member.class);
        return members;
    }

    @UnitOfWork
    public Member getMember() {
        List<Member> members = findAll(Member.class, EnumOrder.ASC, "fullname");
        Member member = members.get(0);
        return member;
    }

    @Transactional
    public Boolean isRegisterSuccess(MemberDto memberDto) {
        boolean status = false;
        try {
            Member member = new Member(memberDto.username, PasswordHash.createHash(memberDto.password), memberDto.fullname, memberDto.birthdate, memberDto.firstname, memberDto.lastname, "01");
            
            save(member);

            member.setCertificate(EncryptUtils.base64encode(member.getId()+"-"+member.getStatus()));
            
            merge(member);
            
            String subject = "verification mail";
            String[] addTo = {member.getUsername()};
            
            MailObj mailObj = new MailObj(subject, "admin@zebravan.co", addTo, "http://localhost:8080/verification/"+member.getCertificate(), "");
            
            mailController.sendMail(mailObj);
            status = true;
        } catch (NoResultException e) {
            //nothing
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return status;
    }

    @Transactional
    public Boolean isRegisterSMSuccess(SNSConnectDto snsConnectDto) {
        boolean status = false;
        try {
            Member member = new Member(snsConnectDto.getEmail(), snsConnectDto.getFirst_name(), snsConnectDto.getLast_name(), snsConnectDto.getId(), snsConnectDto.getStatus());

            save(member);
            
            status = true;
        } catch (Exception e) {
            status = false;
            e.printStackTrace();
        }
        return status;
    }

}

package dao;

/**
 * Created by zikki on 12/08/2015.
 * Edited by saflim on 13/08/2015.
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ninja.jpa.UnitOfWork;
import models.BaseModel;
import models.EnumOrder;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

public class GenericDao {

	/*
	 * A "copy" of the Hibernate's API as this doesn't exist in JPA.
	 */
	public enum MatchMode {
		START, END, EXACT, ANYWHERE
	}

	@Inject
	private Provider<EntityManager> entityManagerProvider;

	public EntityManager getEntityManager() {
		return entityManagerProvider.get();
	}

	/**
	 * Saves an entity.
	 * 
	 * @param entity
	 * @return newly created id for the entity.
	 */
	@Transactional
	public <T extends BaseModel<PK>, PK extends Serializable> PK save(T entity) {
		getEntityManager().persist(entity);
		return entity.getId();
	}
	
	/**
	 * Marges objects with the same identifier within a session into a newly
	 * created object.
	 * 
	 * @param entity
	 * @return a newly created instance merged.
	 */
	@Transactional
	public <T extends BaseModel<PK>, PK extends Serializable> T merge(T entity) {
		return getEntityManager().merge(entity);
	}

	/**
	 * Deletes the entity.
	 * 
	 * @param clazz
	 * @param id
	 * @throws NoResultException
	 *             if the id does not exist.
	 */
	@Transactional
	public <T extends BaseModel<PK>, PK extends Serializable> void delete(
			Class<T> clazz, PK id) {
		T entity = findForDelete(clazz, id);
		if (entity != null) {
			getEntityManager().remove(getEntityManager().contains(entity) ? entity : getEntityManager().merge(entity));
			//getEntityManager().remove(entity);
		} else {
			throw new NoResultException();
		}
	}

	/**
	 * Find an entity by its identifier.
	 * 
	 * @param clazz
	 * @param id
	 * @return
	 */
	@UnitOfWork
	public <T extends BaseModel<?>> T find(Class<T> clazz, Serializable id) {
		return getEntityManager().find(clazz, id);
	}

	/**
	 * Find an entity by its identifier.
	 * 
	 * @param clazz
	 * @param id
	 * @return
	 */
	public <T extends BaseModel<?>> T findForDelete(Class<T> clazz, Serializable id) {
		return getEntityManager().find(clazz, id);
	}

	/**
	 * Finds an entity by one of its properties.
	 * 
	 * 
	 * @param clazz
	 *            the entity class.
	 * @param propertyName
	 *            the property name.
	 * @param value
	 *            the value by which to find.
	 * @return
	 */
	@UnitOfWork
	public <T extends BaseModel<?>> List<T> findByProperty(Class<T> clazz,
			String propertyName, Object value) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(clazz);
		Root<T> root = cq.from(clazz);
		cq.where(cb.equal(root.get(propertyName), value));
		return getEntityManager().createQuery(cq).getResultList();
	}

	/**
	 * Finds entities by a String property specifying a MatchMode. This search
	 * is case insensitive.
	 * 
	 * @param clazz
	 *            the entity class.
	 * @param propertyName
	 *            the property name.
	 * @param value
	 *            the value to check against.
	 * @param matchMode
	 *            the match mode: EXACT, START, END, ANYWHERE.
	 * @return
	 */
	@UnitOfWork
	public <T extends BaseModel<?>> List<T> findByProperty(Class<T> clazz,
			String propertyName, String value, MatchMode matchMode) {
		// convert the value String to lower-case
		value = value.toLowerCase();
		if (MatchMode.START.equals(matchMode)) {
			value = value + "%";
		} else if (MatchMode.END.equals(matchMode)) {
			value = "%" + value;
		} else if (MatchMode.ANYWHERE.equals(matchMode)) {
			value = "%" + value + "%";
		}

		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
		Root<T> root = criteriaQuery.from(clazz);
		
		criteriaQuery.where(criteriaBuilder.like(criteriaBuilder.lower(root.<String>get(propertyName)), value));

		return getEntityManager().createQuery(criteriaQuery).getResultList();
	}

	/**
	 * Finds all objects of an entity class.
	 * 
	 * @param clazz
	 *            the entity class.
	 * @return
	 */
	@UnitOfWork
	public <T extends BaseModel<?>> List<T> findAll(Class<T> clazz) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(clazz);
		cq.from(clazz);
		return getEntityManager().createQuery(cq).getResultList();
	}
	

	/**
	 * Finds all objects of a class by the specified order.
	 * 
	 * @param clazz
	 *            the entity class.
	 * @param order
	 *            the order: ASC or DESC.
	 * @param propertiesOrder
	 *            the properties on which to apply the ordering.
	 * 
	 * @return
	 */
	@UnitOfWork
	public <T extends BaseModel<?>> List<T> findAll(Class<T> clazz,
			EnumOrder order, String... propertiesOrder) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(clazz);
		Root<T> root = cq.from(clazz);

		List<javax.persistence.criteria.Order> orders = new ArrayList<>();
		for (String propertyOrder : propertiesOrder) {
			if (order.isAscOrder()) {
				orders.add(cb.asc(root.get(propertyOrder)));
			} else {
				orders.add(cb.desc(root.get(propertyOrder)));
			}
		}
		cq.orderBy(orders);

		return getEntityManager().createQuery(cq).getResultList();
	}

	/**
	 * Finds the first objects.
	 *
	 * @param clazz
	 *            the entity class.
	 *
	 * @return T
	 */
	@UnitOfWork
	public <T extends BaseModel<?>> T findFirst(Class<T> clazz) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(clazz);
		cq.from(clazz);
		return getEntityManager().createQuery(cq).setMaxResults(1).getSingleResult();
	}

	/**
	 * Finds objects of a class by page
	 *
	 * @param clazz
	 *            the entity class.
	 * @param pageIndex
	 *            page number, start from 0
	 * @param pageSize
	 *            the size of one page
	 *
	 * @return
	 */
	@UnitOfWork
	public <T extends BaseModel<?>> List<T> findByPage(Class<T> clazz,
													Integer pageIndex, Integer pageSize) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(clazz);
		Root<T> root = cq.from(clazz);
		Integer firstResult =  (pageIndex * pageSize)-pageSize;
		if(firstResult < 0){firstResult = 0;}
		return getEntityManager().createQuery(cq)
				.setFirstResult(firstResult)
				.setMaxResults(pageSize)
				.getResultList();
	}
	

    /**
     * Finds ONE entity by a String property EXACT.
     *
     * @param clazz
     *            the entity class.
     * @param propertyName
     *            the property name.
     * @param value
     *            the value to check against.
     * @return
     */
    @UnitOfWork
    public <T extends BaseModel<?>> T findOneByProperty(Class<T> clazz,
                                                        String propertyName, String value){
        try{
    	// convert the value String to lower-case
        value = value.toLowerCase();

        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<T> root = criteriaQuery.from(clazz);

        criteriaQuery.where(criteriaBuilder.equal(criteriaBuilder.lower(root.<String>get(propertyName)), value));

        T a = getEntityManager().createQuery(criteriaQuery).getSingleResult();
        return a;

        }catch(NoResultException n){
        	return null;
        }
    }
    
    @UnitOfWork
	public <T extends BaseModel<?>> int countAll(Class<T> clazz) {
		
		CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(clazz)));
		cq.where(/*your stuff*/);
		Long toret = getEntityManager().createQuery(cq).getSingleResult();
		
		return (int) (long) toret;
	}
  
}

package dto;

import java.util.Date;

import javax.persistence.Transient;


public class MemberDto {
	
	//Member
		public Long id;
		public String username;
		public String password;
		public String fullname;
		public boolean isAdmin;
		public String snsId;
		public String firstname;
		public String lastname;
		public Date birthdate;
		public String gender;
		public boolean fblogin;
		public String salt;
		public String birthdate1;
		//Address
	    public String addresstype;
	    public String addressline1;
	    public String addressline2;
	    public String district;
	    public String city;
	    public String province;
	    public String country;
	    public String postcode;
	    
	    //Member Socmed
	    public String socmedno;
	    public String resfield1;
	    public String resfield2;
	    public String resfield3;
	    public String resfield4;
	    
	    //Contact
	    public String contacttype;
	    public String detail;
	    public String channel;
	
	public MemberDto(){}

	public String getBirthdate1() {
		return birthdate1;
	}

	public void setBirthdate1(String birthdate1) {
		this.birthdate1 = birthdate1;
	}
}

package dto;

/**
 * Created by safaridinsal on 30/07/2015.
 */
public class LookupGroupDto {
    public String code;
    public String description;

    public LookupGroupDto(){}

    public LookupGroupDto(String code, String description) {
        this.code = code;
        this.description = description;
    }

}

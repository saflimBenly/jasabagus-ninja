package dto;

public class CategoryDto {
	public String id;
	public String code;
	public String description;
	public String parent;
	public String level;
}

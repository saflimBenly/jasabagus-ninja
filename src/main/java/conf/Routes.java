/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package conf;

import ninja.AssetsController;
import ninja.Results;
import ninja.Router;
import ninja.application.ApplicationRoutes;
import ninja.utils.NinjaProperties;

import com.google.inject.Inject;

import controllers.AdminController;
import controllers.ApiController;
import controllers.ApplicationController;
import controllers.ArticleController;
import controllers.LoginLogoutController;

public class Routes implements ApplicationRoutes {
    
    @Inject
    NinjaProperties ninjaProperties;

    /**
     * Using a (almost) nice DSL we can configure the router.
     * 
     * The second argument NinjaModuleDemoRouter contains all routes of a
     * submodule. By simply injecting it we activate the routes.
     * 
     * @param router
     *            The default router of this application
     */
    @Override
    public void init(Router router) {

        router.OPTIONS().route("/.*").with(ApplicationController.class, "cors"); //Preflight end point
        router.POST().route("/person").with(ApplicationController.class, "person");

        router.POST().route("/authenticate").with(ApplicationController.class, "authenticate");
        router.GET().route("/getblog/{id}").with(ApplicationController.class, "getBlog");
        router.POST().route("/saveblog").with(ApplicationController.class, "saveBlog");

    
        // puts test data into db:
//        if (!ninjaProperties.isProd()) {
//            router.GET().route("/setup").with(ApplicationController.class, "setup");
//        }
    	router.GET().route("/index").with(ApplicationController.class, "index");
        ///////////////////////////////////////////////////////////////////////
        // Landing Page
        ///////////////////////////////////////////////////////////////////////
        router.GET().route("/landing").with(Results.html().template("/assets/landing/index.html"));
        router.GET().route("/terms").with(Results.html().template("/assets/landing/terms.html"));
        router.GET().route("/policy").with(Results.html().template("/assets/landing/policy.html"));

        router.POST().route("/joinNewsLetter").with(LoginLogoutController.class, "joinNewsLetter");
        ///////////////////////////////////////////////////////////////////////
        // Login / Logout
        ///////////////////////////////////////////////////////////////////////
        router.GET().route("/login").with(LoginLogoutController.class, "login");
        router.POST().route("/login").with(LoginLogoutController.class, "loginPost");
        router.POST().route("/loginSNS").with(LoginLogoutController.class, "loginSNS");
        router.POST().route("/signupSNS").with(LoginLogoutController.class, "signupSNS");
        router.GET().route("/logout").with(LoginLogoutController.class, "logout");
        
        router.GET().route("/sign-up").with(LoginLogoutController.class, "signUp");
        router.POST().route("/sign-up").with(LoginLogoutController.class, "signUpPost");
        router.POST().route("/sign-up-fb").with(LoginLogoutController.class, "signUpFB");
        
        router.GET().route("/verification/{key}").with(LoginLogoutController.class, "verificationAccount");

        router.GET().route("/callback").with(LoginLogoutController.class, "callback");
//        router.POST().route("/loginGoogle").with(LoginLogoutController.class, "loginGoogle");
        ///////////////////////////////////////////////////////////////////////
        // Create new article
        ///////////////////////////////////////////////////////////////////////
//        router.GET().route("/article/new").with(ArticleController.class, "articleNew");
//        router.POST().route("/article/new").with(ArticleController.class, "articleNewPost");
        
        ///////////////////////////////////////////////////////////////////////
        // Create new article
        ///////////////////////////////////////////////////////////////////////
//        router.GET().route("/article/{id}").with(ArticleController.class, "articleShow");

        ///////////////////////////////////////////////////////////////////////
        // Admin
        ///////////////////////////////////////////////////////////////////////
        router.GET().route("/admin").with(AdminController.class, "adminFront");
        
        router.GET().route("/admin/member").with(AdminController.class, "memberList");
        router.GET().route("/admin/memberDetail/{id}").with(AdminController.class, "memberDetail");
        router.POST().route("/admin/updateMember").with(AdminController.class, "updateMember");
        router.POST().route("/uploadImage").with(AdminController.class, "uploadImage");
        router.GET().route("/retriveImage/{id}").with(AdminController.class, "retriveImage");
        
        router.GET().route("/admin/lookupGroupList").with(AdminController.class, "lookupGroupList");
        router.POST().route("/admin/addLookupGroup").with(AdminController.class, "addLookupGroup");
        router.POST().route("/admin/deleteLookupGroup").with(AdminController.class, "deleteLookupGroup");
        router.POST().route("/admin/updateLookupGroup").with(AdminController.class, "updateLookupGroup");
        
        router.GET().route("/admin/lookupList").with(AdminController.class, "lookupList");
        router.POST().route("/admin/addLookup").with(AdminController.class, "addLookup");
        router.POST().route("/admin/deleteLookup").with(AdminController.class, "deleteLookup");
        router.POST().route("/admin/updateLookup").with(AdminController.class, "updateLookup");

        router.GET().route("/admin/categoryList").with(AdminController.class, "categoryList");
        
        // Search
        router.GET().route("/search").with(ApplicationController.class, "search");
        
        
        ///////////////////////////////////////////////////////////////////////
        // Api for management of software
        ///////////////////////////////////////////////////////////////////////
        router.GET().route("/api/admin/memberDetail/{id}").with(ApiController.class, "getMemberDetailJson");
        router.GET().route("/api/{username}/articles.json").with(ApiController.class, "getArticlesJson");
//        router.GET().route("/api/{username}/article/{id}.json").with(ApiController.class, "getArticleJson");
//        router.GET().route("/api/{username}/articles.xml").with(ApiController.class, "getArticlesXml");
//        router.POST().route("/api/{username}/article.json").with(ApiController.class, "postArticleJson");
//        router.POST().route("/api/{username}/article.xml").with(ApiController.class, "postArticleXml");
 
        ///////////////////////////////////////////////////////////////////////
        // Assets (pictures / javascript)
        ///////////////////////////////////////////////////////////////////////    
        router.GET().route("/assets/webjars/{fileName: .*}").with(AssetsController.class, "serveWebJars");
        router.GET().route("/assets/{fileName: .*}").with(AssetsController.class, "serveStatic");
        
        ///////////////////////////////////////////////////////////////////////
        // Index / Catchall shows index page
        ///////////////////////////////////////////////////////////////////////
        router.GET().route("/.*").with(Results.html().template("/assets/landing/index.html"));
    }

}
